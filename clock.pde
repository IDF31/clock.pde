/*   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

int hour_length = 60;          //length of the hour arrow
int minute_length = 80;        //length of the minute arrow
int second_length = 100;       //length of the second arrow

int clock_radius = 125;        //radius of the clock (used for space between the numbers)

boolean roman = false;         //"true" for roman numbers, "false" for arabic numbers(default)
String[] roman_nr = { "XII", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI" };     //array of roman numbers
String[] arabic_nr = { "12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};                //array of arabic numbers

void setup() {
  //fullScreen();     //uncomment for fullscreen
  size(300, 300);     //comment for fullscreen
  stroke(255);
  smooth();
}

void drawSecond() {
  stroke(0, 0, 255);
  line(width/2, height/2, width/2+cos(radians((6*second())-90))*second_length, height/2+sin(radians((6*second())-90))*second_length);
  
}

void drawMinute() {
  stroke(255, 255, 0);
  line(width/2, height/2, width/2+cos(radians((6*minute())-90))*minute_length, height/2+sin(radians((6*minute())-90))*minute_length);
}

void drawHour() {
  stroke(255, 0, 0);
  line(width/2, height/2, width/2+cos(radians((30*hour())-90))*hour_length, height/2+sin(radians((30*hour())-90))*hour_length);
}

void drawNumbers() {
  
  String[] numbers;
  
  if (roman) {
    numbers = roman_nr;
  }
  else {
    numbers = arabic_nr;
  }
  
  stroke(255);
  for (int nr = 0; nr < 12; nr++) 
    text(numbers[nr], width/2+cos(radians((30*nr)-90))*clock_radius, height/2+sin(radians((30*nr)-90))*clock_radius);
}

void draw() {
  
  //clear the screen
  clear();
  
  //draw the clock
  drawNumbers();
  drawHour();
  drawMinute();
  drawSecond();
  
}

void mouseClicked() {
  
  //toggle between roman and arabic numbers
  roman = !roman;
}
